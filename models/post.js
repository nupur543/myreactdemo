class Post {
  constructor(id, userId, name, imageUrl, description, likedUsers = [], comments = [Comment]) {
    this.id = id;
    this.userId = userId;
    this.name = name;
    this.imageUrl = imageUrl;
    this.description = description;
    this.likedUsers = likedUsers;
    this.comments = comments;
  }
}

export default Post;

