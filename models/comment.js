class Comment {
    constructor(username, commentText) {
      this.username = username;
      this.commentText = commentText;
    }
  }
  
  export default Comment;
