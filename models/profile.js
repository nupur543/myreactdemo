class Profile {
    constructor( userId, name, imageUrl) {
    
      this.userId = userId;
      this.name = name;
      this.imageUrl = imageUrl;
    }
  }
  
  export default Profile;
  