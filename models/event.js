class Event {
    constructor(userId,id, name, imgURL, eventURL) {
      this.userId = userId;
      this.id = id;
      this.name = name;
      this.imgURL = imgURL;
      this.eventURL = eventURL;
    }
  }
  
  export default Event;
  