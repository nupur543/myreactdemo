import React, { useEffect, useCallback, useReducer, useState } from 'react';
import {
  View,
  StyleSheet,
  Platform,
  Alert,
  FlatList
} from 'react-native';
import { useSelector, useDispatch } from 'react-redux';

import * as favouritesActions from '../store/actions/favourites';
import AttractionCell from '../components/UI/AttractionCell';
import { HeaderButtons, Item } from 'react-navigation-header-buttons';
import HeaderButton from '../components/UI/HeaderButton';

const MyFavourites = props => {

 const attractions = useSelector(state => state.fav.favourites);
  const dispatch = useDispatch();
  
  useEffect(() => {
    dispatch(favouritesActions.loadAttractions());
  }, [dispatch]);

  // console.log("Attraction ");
  // console.log(attractions);

  FlatListItemSeparator = () => {
    return (
      <View
        style={{
          height: 1.5,
          width: "100%",
          backgroundColor: "#A9A9A9",
          marginVertical : 10
        }}
      />
    );
  }

  const deleteAttraction = id => {
    console.log("deleteAttraction called");
    Alert.alert(
      'Delete Address',
      'Are you sure want to delete this address ?',
      [
        {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
        {text: 'OK', onPress: () =>  dispatch(favouritesActions.deleteAttraction(id))},
      ],
      { cancelable: false }
    )
  };

   return <FlatList
    data={attractions}
    keyExtractor={item => item.id}
    contentContainerStyle={{ paddingBottom: 30}}
    renderItem={itemData => 
      (
         <AttractionCell
      image={itemData.item.imgURL}
      attraction={itemData.item}
      onSelect={() => {
         deleteAttraction(itemData.item.id)}
         }
      />
    )}
    ItemSeparatorComponent = {FlatListItemSeparator }
  />
}

MyFavourites.navigationOptions  = navData => {
  
  return {
    headerTitle: 'My Favourites',
    headerLeft: (
      <HeaderButtons HeaderButtonComponent={HeaderButton}>
        <Item
          title="Menu"
          iconName={Platform.OS === 'android' ? 'md-menu' : 'ios-menu'}
          onPress={() => {
            navData.navigation.toggleDrawer();
          }}
        />
      </HeaderButtons>
    )
  };
};


// const styles = StyleSheet.create({
// }
// );

export default MyFavourites;