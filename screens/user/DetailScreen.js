import React, { useEffect, useState } from 'react';
import {
  View,
  SafeAreaView,
  FlatList,
  Text,
  KeyboardAvoidingView,
  TextInput,
  ActivityIndicator,
  StyleSheet,
  ScrollView,
  Image,
  Button,
  AsyncStorage,
  Linking
} from 'react-native';
import ImageSlider from 'react-native-image-slider';
import { useDispatch,useSelector } from 'react-redux';
import * as favouritesActions from '../../store/actions/favourites';
import { HeaderButtons, Item } from 'react-navigation-header-buttons';
import HeaderButton from '../../components/UI/HeaderButton';
import AttractionCell from '../../components/UI/AttractionCell';
import * as authActions from '../../store/actions/auth';

const DetailScreen = props => {
 
  const imageURL = props.navigation.getParam('imageURL');
  const eventId = props.navigation.getParam('eventId');
  const [hasError, setErrors] = useState(false);
  const [attraction, setAttraction] = useState({});
  const [imageArray, setImageArray] = useState([]);
  const ifFavourite = useSelector(state =>
    state.fav.favourites.find(event => event.id === eventId) 
  );

  const dispatch = useDispatch();
  
  useEffect(() => {
    dispatch(favouritesActions.loadAttractions());
  }, [dispatch]);

  useEffect(() => {
    fetchData();
  }, [] );

  useEffect(() => {
    props.navigation.setParams({ markFavourite: addToFavourites});
    props.navigation.setParams({ isFav: ifFavourite});
  }, [addToFavourites, attraction, ifFavourite]);


  async function fetchData() {
    console.log("fetchData");
    const url = `https://app.ticketmaster.com/discovery/v2/attractions/${eventId}.json?apikey=A5GGV53qMPDr6r5IJElGG8MUQvAb70Gi`;
      const res = await fetch(url);
      res
        .json()
        .then(res => {
          //console.log("Attractn response");
          //console.log(res);
          setAttraction(res)
          const arr = [];
          res.images.forEach(element => {
            arr.push(element.url);
          });
          setImageArray(arr);
          
    })
        .catch(err => setErrors(err));
  }

  const addToFavourites = async () => {
    
    if (ifFavourite) {
      return
    }
    dispatch(favouritesActions.addEvent(eventId,attraction.name, imageURL, attraction.url));
    props.navigation.goBack();
  };

 return (
      <AttractionCell
      image={imageURL}
      attraction={attraction}
      />
     // <ImageSlider style={{flex: 0 ,width: '100%', height: 270}} images={imageArray}/>
  );
};


DetailScreen.navigationOptions  = navData => {

  const markFavourite = navData.navigation.getParam('markFavourite');
  const isFavourite = navData.navigation.getParam('isFav');
   //console.log("ifFavourite exists");
   //console.log(isFavourite);
  return {
  headerTitle: 'Event Details',
  headerRight: (
    <HeaderButtons HeaderButtonComponent={HeaderButton}>
      <Item
        title="Favourite"
        iconName= { isFavourite ? 'ios-star-outline' : 'ios-star'}
        onPress={markFavourite}
      />
    </HeaderButtons>
  )
  };
};

const styles = StyleSheet.create({

  
});

export default DetailScreen;
