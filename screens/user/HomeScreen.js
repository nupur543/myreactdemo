import React, { useEffect, useState } from 'react';
import {
  View,
  SafeAreaView,
  FlatList,
  Text,
  KeyboardAvoidingView,
  TextInput,
  ActivityIndicator,
  StyleSheet,
  TouchableOpacity,
  AsyncStorage,
  Button,
  Image,
  Platform
} from 'react-native';

import Input from '../../components/UI/Input';
import { useDispatch } from 'react-redux';
import { HeaderButtons, Item } from 'react-navigation-header-buttons';

import HeaderButton from '../../components/UI/HeaderButton';
import Colors from '../../constants/Colors';
import Card from '../../components/UI/Card';

const HomeScreen = props => {
 
 const dispatch = useDispatch();
 const [keyword, setKeyword] = useState('');
 const [isLoading, setIsLoading] = useState(false);
 const [isTyping, setIsTyping] = useState(false);
 const [error, setErrors] = useState();
 const [data, setData] = useState('');
 
function Item({ attraction }) {

  return (

    <Card style={styles.product}>
      <View style={styles.touchable}>
        <TouchableOpacity onPress={() => 
         props.navigation.navigate('Detail', {
          imageURL: attraction.images[0].url ,
          eventId: attraction.id
        })
          } useForeground>
        
          <Text style={styles.title}>{attraction.name}</Text>
            <View style={styles.imageContainer}>
              <Image style={styles.image} source={{ uri:attraction.images[2].url }} />
            </View>
            <View style={styles.details}>
              <Text style={styles.price}>{`Genre - ${attraction.classifications[0].genre.name}`}</Text>
              <Text style={styles.price}>{`Sub Genre - ${attraction.classifications[0].subGenre.name}`}</Text>
            </View>
      
        </TouchableOpacity>
      </View>
    </Card>
  );
}

fetchEvents = () => {

  if (keyword.length == 0) {
    return
  }
  setIsLoading(true);
  const url =  `https://app.ticketmaster.com/discovery/v2/attractions.json?keyword=${keyword}&apikey=A5GGV53qMPDr6r5IJElGG8MUQvAb70Gi`;
  fetch(url)
    .then(res => res.json())
    .then(res => {

      setIsLoading(false);
      setIsTyping(false);
     if (res._embedded) {
     
      setData(res._embedded.attractions);
      console.log(data.length);
      
     }else{

      setData([]);
      console.log("Attractn not parsed:"); 
     }
    
    })
    .catch(error => {
      console.log("get data error:" + error);
      setErrors(error)
      if (error) {
        return (
          <View style={styles.centered}>
            <Text>An error occurred!</Text>
          </View>
        );
      }
    });
}


if (isLoading) {
  return (
    <View style={styles.centered}>
      <ActivityIndicator size="large" color={Colors.primary} />
    </View>
  );
}

  return (
    <SafeAreaView style={styles.container}>
      <TextInput style={styles.inputSearch}
        placeholder = "Search for events" 
        onChangeText={searchInput => {
          setKeyword(searchInput);
          setData([]);
          setIsTyping(true);
        }}
        onSubmitEditing={
          fetchEvents}
        value = {keyword}
      />
      <Text style={styles.titleRecent}>Event Results</Text>
      {
      
      (data.length > 0 && isTyping == false) ?
      <FlatList
        data={data}
        extraData={data}
        renderItem={({ item }) => 
        <Item attraction={item} />
      }
        keyExtractor={item => item.id}
      />  
      :
     (keyword.length > 0 && data.length == 0 && isTyping == false) ?
     
     <View style={styles.centered}>
      <Text>No events found with this keyword!</Text>
     </View> : null
    }
    </SafeAreaView>
  );
};

HomeScreen.navigationOptions  = navData => {
  return {
    headerTitle: 'Events',
    headerLeft: (
      <HeaderButtons HeaderButtonComponent={HeaderButton}>
        <Item
          title="Menu"
          iconName={Platform.OS === 'android' ? 'md-menu' : 'ios-menu'}
          onPress={() => {
            navData.navigation.toggleDrawer();
          }}
        />
      </HeaderButtons>
    )
  };
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop:20,
  },
  screen: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  inputSearch : {
    height : 40,
    marginVertical: 10,
    marginHorizontal : 10,
    borderColor: 'gray',
    borderBottomWidth: 1,
    borderRadius: 10,
  },
  item: {
    backgroundColor: 'gray',
    padding: 10,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  title: {
    fontSize: 15,
    marginHorizontal : 10
  },
  titleRecent: {
    fontSize: 19,
    textAlign: "center"
  },
  imageContainer: {
    width: '100%',
    height: '60%',
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    borderBottomEndRadius:10,
    overflow: 'hidden'
  },
  image: {
    width: '100%',
    height: '100%'
  },
  details: {
    alignItems: 'center',
    height: '25%',
    padding: 5
  },
  title: {
    fontFamily: 'open-sans-bold',
    fontSize: 18,
    paddingHorizontal: 10
  },
  price: {
    fontFamily: 'open-sans',
    fontSize: 14,
    color: '#888'
  },
  product: {
    height: 220,
    margin: 20
  },
  touchable: {
    borderRadius: 10,
    overflow: 'hidden'
  },
  centered: { flex: 1, justifyContent: 'center', alignItems: 'center' }
});

export default HomeScreen;
