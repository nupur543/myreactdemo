import {
    View,
    Text,
    StyleSheet,
    Image,
    FlatList,
    Button,
    TextInput,
    Alert
  } from 'react-native';
  import React, { useEffect, useState,useCallback } from 'react';
import { useDispatch,useSelector } from 'react-redux';
import * as postsActions from '../../store/actions/posts';
import * as profileActions from '../../store/actions/profile';
import Colors from '../../constants/Colors';
import Comment from '../../models/comment';
  
const Comments = props => {

    const [txtComment, setTxtComment] = useState('');

    const post = props.navigation.getParam('post');
    const myUserId = props.navigation.getParam('myUserId');
  
    const postComments = useSelector(state => state.posts.availablePosts.filter(
      p =>
      p.id === post.id
    )[0].comments );
  
    const dispatch = useDispatch();

   useEffect(() => {  
    dispatch(postsActions.fetchPosts(myUserId));
   
    },[]);

  const addNewComment = async () => {

        console.log('addNewComment');     
      if (txtComment.length > 0){
        console.log('IF ENTERED');
        dispatch(postsActions.addComment(post.userId, post.id, myUserId, txtComment));
        setTxtComment('');
       
     }else{
      
         Alert.alert(
          'Error!',
          'Enter something', 
          [
            {text: 'OK'},
          ],
          {cancelable: false},
        );
      }
    };

    function Item({ item }) {

        return (
         <View style = {{flexGrow: 0}}>
        <Text style={styles.input}>{`${item.commentText}`}</Text>
        <Text style={styles.input2}>{`${item.username}`}</Text>
        </View>
        );
      }

    return ( 
        <View style={{flex: 1}}>
       <View style={{ marginBottom: 50}}>
        <FlatList
    data={postComments}
    extraData={postComments}
    keyExtractor={item => item}
    renderItem={itemData => 
      (
        <Item item={itemData.item} />
    )}
  />
  </View>
    <View style = {styles.bottom}>
       <TextInput style={styles.inputSearch}
        placeholder = "Enter your comment" 
        onChangeText={searchInput => {
            setTxtComment(searchInput);
        }}
        value = {txtComment}
        clearButtonMode='always'
        multiline = {true}
       />
        <Button
             title={'Add comment'}
             style = {styles.commentBtn}
             color={Colors.primary}
             onPress={addNewComment}
         />
    </View>
    </View> );
};

Comments.navigationOptions  = navData => {
    return {
    headerTitle: 'Comments'
    };
};


const styles = StyleSheet.create({
    input: {
        paddingHorizontal: 2,
        paddingVertical: 10,
        fontFamily: 'open-sans-bold',
        fontSize : 18,
        width : '100%',
        textAlign:'left'
      },
      input2: {
        paddingHorizontal: 2,
        paddingVertical: 10,
        borderBottomColor: '#ccc',
        borderBottomWidth: 1,
        fontSize : 15,
        fontFamily: 'open-sans',
        width : '100%',
        textAlign:'right'
      },
 bottom: {
    position: 'absolute',
    bottom:0,
    flex: 1,
    flexDirection:"row",
     paddingHorizontal: 5, 
     justifyContent: 'center', 
     alignItems: 'center'
  },
  commentBtn : {
    height : 40,
    width: '15%',
    borderBottomWidth: 1,
    borderRadius: 10,
    paddingRight:10
  },
  inputSearch : {
    height : 40,
   // marginVertical: 10,
 backgroundColor : '#DCDCDC',
    borderColor: 'gray',
    borderBottomWidth: 1,
   // borderRadius: 10,
    width: '70%'
  },
    
});

export default Comments;