import React, { useEffect, useState, useCallback } from 'react';
import {
  View,
  Text,
  TextInput,
  ActivityIndicator,
  StyleSheet,
  FlatList,
  TouchableOpacity,
  AsyncStorage,
} from 'react-native';

import 'firebase/storage';
import { useDispatch,useSelector } from 'react-redux';
import * as profileActions from '../../store/actions/profile';

const LikedByUsers = props => {
  const usersList = props.navigation.getParam('usersList');
  const usersProfiles = useSelector(state => 
    state.profile.likedProfiles
);

  const dispatch = useDispatch();

  useEffect(() => {
   
    dispatch(profileActions.fetchProfile(''));
},[dispatch]);

  const newusersList = usersList.filter(
    user =>
    user !== 'null' );
    
    const viewProfile = async (user) => {
     
      props.navigation.navigate('Profile', {
        userProfile: user 
      });
    };

    console.log(usersProfiles);
  
    function Item({ item }) {

      const userProfl = usersProfiles.filter(
        profl =>
        profl.userId == item
      );
    const profl = userProfl[0];
      // console.log('userProfl.name');
      // console.log(profl);

      return (
    <TouchableOpacity onPress={() => viewProfile(profl)}>
    <Text style={styles.input}>{profl ? profl.name : ""}</Text>
    </TouchableOpacity>
      );
    }
    
    return  <FlatList
    data={newusersList}
    extraData={newusersList}
    keyExtractor={item => item}
    renderItem={({ item }) => 
    <Item item={item} />
    }
  /> 
}

LikedByUsers.navigationOptions  = navData => {
    return {
    headerTitle: 'Liked by Users'
    };
  };

const styles = StyleSheet.create({
    input: {
        paddingHorizontal: 2,
        paddingVertical: 30,
        borderBottomColor: '#ccc',
        borderBottomWidth: 1,
        fontSize : 20,
        width : '100%',
        textAlign:'center'
      }
    });

export default LikedByUsers;