
import React, { useEffect, useCallback, useReducer, useState,useLayoutEffect } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image
} from 'react-native';

const UserProfile = props => {

 const userProfile = props.navigation.getParam('userProfile');

return userProfile ? <View style={{alignItems: 'center', margin: 20, flex: 1, flexDirection : 'column'}}>
          <Image style={styles.avatar} source={{ uri: userProfile.imageUrl }} /> 
            <Text style={styles.label}>{userProfile.name}</Text>
          </View> : null
}

UserProfile.navigationOptions  = navData => {
    return {
    headerTitle: 'User Profile'
    };
  };

const styles = StyleSheet.create({
    avatar : {
        width: 160,
        height :160,
        borderRadius: 80
      },
    label: {
        fontFamily: 'open-sans-bold',
        marginVertical: 20,
        fontSize : 18
      }
    });

export default UserProfile;