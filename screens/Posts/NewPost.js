import React, { useEffect, useState,useCallback } from 'react';
import {
  View,
  Text,
  TextInput,
  ActivityIndicator,
  StyleSheet,
  ScrollView,
  Image,
  Alert,
  Button,
  TouchableOpacity,
  AsyncStorage,
} from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import * as ImagePicker from 'expo-image-picker';
import * as Permissions from 'expo-permissions';
import * as firebase from 'firebase/app';
import 'firebase/storage';
import { useDispatch,useSelector } from 'react-redux';
import * as postsActions from '../../store/actions/posts';
import Colors from '../../constants/Colors';

const NewPost = props => {

    const [isLoading, setIsLoading] = useState(false);
    const [name, setName] = useState("")
    const [description, setDescription] = useState("")
    const [downloadedImgURL, setDownloadedImgURL] = useState("")
    const [selectedImage, setSelectedImage] = useState(null);
    const dispatch = useDispatch();
   
    const uploadImage = async () => {

        setIsLoading(true);
        if (selectedImage ) {
              const response = await fetch(selectedImage);
              const blob = await response.blob()
              const storageRef = firebase.storage().ref('Posts');
              const timestamp = Date.now().toString();
            const thisRef = storageRef.child(timestamp);
            thisRef.put(blob)
            .then(function (snapshot) {
            console.log('Uploaded a blob or file!');
            thisRef.getDownloadURL().then((url, error) =>  updatePost(url,timestamp))
              
            }); 
        }else{
            Alert.alert('Select the Image to upload.', [
                { text: 'Okay' }
              ]);
        } 
      };

      const updatePost = async (imgURL, timestamp) => {
        
        const userData = await AsyncStorage.getItem('userData');
            if (!userData) {
              setIsLoading(false);
              props.navigation.navigate('Auth');
              return;
            }
            
            const transformedData = JSON.parse(userData);
            const { token, userId, expiryDate } = transformedData;
         
        dispatch(postsActions.createPost(timestamp, userId, name, description,imgURL));
        props.navigation.goBack();
      }

    const addNewPost = async () => {

        if ((name && name.length > 0) && (description && description.length> 0) && (selectedImage && selectedImage.length > 0) ){
     
            uploadImage();
      }else{
      
        Alert.alert('Wrong input!', 'Please check the errors in the inputs.', [
          { text: 'Okay' }
        ]);
      }
    };

    handlePicAvatar = useCallback(async () => {

        const { status: cameraRollPerm } = await Permissions.askAsync(
          Permissions.CAMERA_ROLL
        );
    
        if (cameraRollPerm === "granted") {
        let pickerResult = await ImagePicker.launchImageLibraryAsync({
          mediaTypes: ImagePicker.MediaTypeOptions.Images,
          allowsEditing: true,
          aspect: [4, 3],
          quality: 1,
        });
    
        if (pickerResult.cancelled === true) {
          return;
        }
    
        setSelectedImage( pickerResult.uri);
        setDownloadedImgURL('');
        // console.log('Picked Image:');
        // console.log(pickerResult.uri);
      }else{
    
        Alert.alert(
          'Insufficient permissions!',
          'You need to grant camera permissions to use this app.',
          [{ text: 'Okay' }]
        );
      }
      });
  
    return (
        <ScrollView>
        <View style={styles.form}>
        <View style={{alignItems: 'center'}}>
          <TouchableOpacity style={styles.avatarPlaceholder} onPress={handlePicAvatar} >
        
          <Image style={styles.avatar} source={{ uri: downloadedImgURL ? downloadedImgURL : selectedImage? selectedImage : null }} /> 
         
          {
           (!(downloadedImgURL && downloadedImgURL.length > 0) && !(selectedImage && selectedImage.length > 0) )&&
           <Ionicons
           name = "ios-add"
           size = {40}
           color = "#FFF"
           style = {{marginTop: 2, marginLeft: 6}}
         >
         </Ionicons>
}
          </TouchableOpacity>
          {isLoading && 
                <ActivityIndicator size="small" color={Colors.primary} />
           }
        <TextInput
          style={styles.input}
          returnKeyType="next"
          placeholder="Enter the title"
          onChangeText={text => setName(text)}
          value={name}
         
        />
        <TextInput
          style={styles.input}
          returnKeyType="next"
          placeholder="Enter the description"
          onChangeText={text => setDescription(text)}
          value={description}
         multiline = {true}
        />
        <View style={styles.buttonContainer}></View>
        <Button
             title={'Add new post'}
             color={Colors.primary}
             onPress={addNewPost}
         />
          </View>
        </View>
      </ScrollView>
      );
};

NewPost.navigationOptions  = navData => {
    return {
    headerTitle: 'Add New Post'
    };
  };

const styles = StyleSheet.create({
    form: {
      margin: 20
    },
  
    formControl: {
      width: '100%'
    },
    label: {
      fontFamily: 'open-sans-bold',
      marginVertical: 20,
      fontSize : 18
    },
    input: {
      paddingHorizontal: 2,
      paddingVertical: 30,
      borderBottomColor: '#ccc',
      borderBottomWidth: 1,
      fontSize : 20,
      width : '80%',
      textAlign:'center'
    },
    errorContainer: {
      marginVertical: 5
    },
    errorText: {
      fontFamily: 'open-sans',
      color: 'red',
      fontSize: 13
    },
    avatarPlaceholder: {
     
      width: 160,
      height :160,
      borderRadius: 80,
      marginTop: 10,
      backgroundColor: '#E1E2E6',
      justifyContent: 'center',
      alignItems: 'center'
    },
    avatar : {
      width: 160,
      height :160,
      borderRadius: 80,
      position: 'absolute',
    },
    buttonContainer: {
        marginTop: 30
      }
   
  });

  export default NewPost;