import React, { useEffect, useState, useCallback } from 'react';
import {
  View,
  Alert,
  FlatList,
  Text,
  ActivityIndicator,
  StyleSheet,
  Platform,
  Image,
  TouchableOpacity
} from 'react-native';

import {getUserId} from '../../components/utilities/config' 
import { useDispatch,useSelector } from 'react-redux';
import * as postsActions from '../../store/actions/posts';
import { HeaderButtons, Item } from 'react-navigation-header-buttons';
import HeaderButton from '../../components/UI/HeaderButton';
import Colors from '../../constants/Colors';
import Card from '../../components/UI/Card';

const PostsScreen = props => {

  const [isLoading, setIsLoading] = useState(false);
  const [isRefreshing, setIsRefreshing] = useState(false);
  const [error, setError] = useState();
  const [myUserId, setMyUserId] = useState('');
  const posts = useSelector(state => state.posts.availablePosts);
  
  getUserId().then((keyValue) => {
    setMyUserId(keyValue);
    });
  const dispatch = useDispatch();

  useEffect(() => {
    props.navigation.setParams({ addPost: addNewPost});
  }, [addNewPost]);

  const loadPosts =  useCallback( () => {
    setError(null);
    setIsRefreshing(true);
    setIsLoading(true);
    try {
      
      dispatch(postsActions.fetchPosts(myUserId));
      setIsLoading(false);
    } catch (err) {
      setError(err.message);
    }
    setIsRefreshing(false);
  },[dispatch, setIsLoading, setError]);

  useEffect(() => {
    const willFocusSub = props.navigation.addListener(
      'willFocus',
      loadPosts
    );

    return () => {
      willFocusSub.remove();
    };
  }, [loadPosts]);

  useEffect(() => {
    setIsLoading(true);
    loadPosts()
      setIsLoading(false);
    
  }, [dispatch, loadPosts]);

  const addNewPost = async () => {
    props.navigation.navigate('NewPost');
  };

   const viewUsers = async (post) => {
   // console.log('Users list');
   // console.log(post.likedUsers);
    props.navigation.navigate('LikedByUser', {
      usersList: post.likedUsers 
    });
  };

  const viewComments = (post) => {
    
    props.navigation.navigate('Comments', {
      post: post ,
      myUserId: myUserId
    });
  };

  const likePressed = (post) => {
    console.log('likePressed'); 
    //console.log('change likedUsers of ' + post.name); 
    dispatch(postsActions.likePost(post.id, myUserId, post.likedUsers));
  };

  const deleteMyPost = post => {
   if (post.userId == myUserId) {

    Alert.alert(
      'Delete Address',
      'Are you sure want to delete this post ?',
      [
        {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
        {text: 'OK', onPress: () =>  dispatch(postsActions.deletePost(myUserId,post.id))},
      ],
      { cancelable: false }
    )
   }else{
     return
   }
    
  };

  function Item({ post }) {
    //console.log('Item POST');
    // console.log('post.likedUsers of ' + post.name); 
    // console.log(post.likedUsers); 

    return (
      <TouchableOpacity onLongPress ={() => {
        deleteMyPost(post)}
        } useForeground>
      <Card style={styles.product}>
             <View style={styles.imageContainer}>
             <Image style={styles.image} source={{ uri:post.imageUrl}} />
               </View>
              <View style={styles.details}>
              <Text style={styles.title}>{post.name}</Text>
                <Text style={styles.price}>{post.description}</Text>
                </View>
                <View style={{flex: 1,flexDirection:"row", paddingHorizontal: 10, justifyContent: 'space-between', alignItems: 'center', marginVertical: 10}}>
                { post.userId !== myUserId ?
              <TouchableOpacity onPress={() => 
              {
                console.log('onPress selected');
                likePressed(post)
              
              }
                
               }>
                {
                  post.likedUsers.includes(myUserId) ? 
               <Image style={styles.imageSize} source={require('../Images/like.png') } />
               :
                <Image style={styles.imageSize} source={require('../Images/unlike.png') } />
                }
              </TouchableOpacity> : null
              }
              
               { 
               (post.likedUsers.length == 1 && post.likedUsers[0] == 'null') ?
                 null :
                 <TouchableOpacity onPress={() => viewUsers(post)}>
                <View>
               <Image style={styles.imageSize} source={require('../Images/eye.png')} />
               </View>
                </TouchableOpacity> 
                }

                { post.userId !== myUserId ?
                <TouchableOpacity onPress={() => viewComments(post)}>
                <View>
               <Image style={styles.imageSize} source={require('../Images/comment.png')} />
               </View>
                </TouchableOpacity> : null
                }
               
              </View>
             
      </Card>
      </TouchableOpacity>
    );
  }
  

  if (isLoading) {
    return (
      <View style={styles.centered}>
        <ActivityIndicator size="large" color={Colors.primary} />
      </View>
    );
  }

 //console.log('loadedPosts');
 //console.log(posts);

  if (!isLoading && posts.length === 0) {
    return (
      <View style={styles.centered}>
        <Text>No posts found. Maybe start adding some!</Text>
      </View>
    );
  }

 return (
   <FlatList
   data={posts}
   extraData={posts}
   renderItem={({ item }) => 
   <Item post={item} />
    }
   keyExtractor={item => item.id}
 /> 
     // <ImageSlider style={{flex: 0 ,width: '100%', height: 270}} images={imageArray}/>
  );
};

PostsScreen.navigationOptions  = navData => {

  const addPost = navData.navigation.getParam('addPost');

  return {
  headerTitle: 'Posts',
  headerLeft: (
      <HeaderButtons HeaderButtonComponent={HeaderButton}>
        <Item
          title="Menu"
          iconName={Platform.OS === 'android' ? 'md-menu' : 'ios-menu'}
          onPress={() => {
            navData.navigation.toggleDrawer();
          }}
        />
      </HeaderButtons>
    ),
  headerRight: (
    <HeaderButtons HeaderButtonComponent={HeaderButton}>
      <Item
        title="Add"
        iconName= {'ios-add'}
        onPress={addPost}
      />
    </HeaderButtons>
  )
  };
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop:20,
  },
  screen: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
 
  item: {
    backgroundColor: 'gray',
    padding: 10,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  title: {
    fontSize: 15,
    marginHorizontal : 10
  },
  titleRecent: {
    fontSize: 19,
    textAlign: "center"
  },
  imageContainer: {
    width: '100%',
    height: 120,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    borderBottomEndRadius:10,
    overflow: 'hidden'
  },
  image: {
    width: '100%',
    height: '100%'
  },
  imageSize :{
    width: 30,
    height: 30
  },
  details: {
    alignItems: 'center',
  },
  
  title: {
    fontFamily: 'open-sans-bold',
    fontSize: 18,
    paddingHorizontal: 10,
    textAlign: 'center'
  },
  price: {
    fontFamily: 'open-sans',
    fontSize: 14,
    textAlign: 'center',
    color: '#888'
   // flexShrink: 1
   // flex: 1
  },
  product: {
    //flexDirection: 'row',
    flexGrow : 0,
   //height: 200,
    margin: 20
  },
  touchable: {
    borderRadius: 10,
    overflow: 'hidden'
  },
  centered:
   { flex: 1,
     justifyContent: 'center',
    alignItems: 'center' }
  
});

export default PostsScreen;
