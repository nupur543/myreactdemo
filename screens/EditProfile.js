import React, { useEffect, useCallback, useState } from 'react';

import {
  View,
  TextInput,
  Text,
  ScrollView,
  StyleSheet,
  Platform,
  Alert,
  AsyncStorage,
  KeyboardAvoidingView,
  TouchableOpacity,
  ActivityIndicator,
  Image
} from 'react-native';

import { Ionicons } from '@expo/vector-icons';
import * as ImagePicker from 'expo-image-picker';
import * as Permissions from 'expo-permissions';
import * as firebase from 'firebase/app';
import 'firebase/storage';
import Firebase from '../components/utilities/config' 
import { useDispatch,useSelector } from 'react-redux';
import * as profileActions from '../store/actions/profile';
import Colors from '../constants/Colors';
import { HeaderButtons, Item } from 'react-navigation-header-buttons';
import HeaderButton from '../components/UI/HeaderButton';

const EditProfile = props => {

  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState();
  const [isTyping, setIsTyping] = useState(false);
  const [name, setName] = useState("")
  const [downloadedImgURL, setDownloadedImgURL] = useState("")
  const [selectedImage, setSelectedImage] = useState(null);
  const usersProfile = useSelector(state => 
    state.profile.profileData
   );
  const dispatch = useDispatch();
 
  useEffect(() => {
    const willFocusSub = props.navigation.addListener(
      'willFocus',
      getProfile
    );
    return () => {
      willFocusSub.remove();
    };
  }, [getProfile]);

  useEffect(() => {
    getProfile()
    
 }, [dispatch,getProfile]);

  useEffect(() => {
    props.navigation.setParams({ submit: editProfilePressed });
 
  }, [name, selectedImage]);

 
   if (usersProfile && ((name.length == 0) || (!isTyping && usersProfile.name !== name )) )  {
     setName(usersProfile.name);
    }

   const getProfile = useCallback( async () => {
  
    setIsTyping(false);
    setError(null);
    setSelectedImage(null);

    const userData = await AsyncStorage.getItem('userData');
        if (!userData) {
          setIsLoading(false);
          props.navigation.navigate('Auth');
          return;
        }
        
        const transformedData = JSON.parse(userData);
        const { token, userId, expiryDate } = transformedData;
     try {
        dispatch(profileActions.fetchProfile(userId));
      
      } catch (err) {
        setError(err.message);
      }
  },[dispatch, setIsLoading,setError]);


  const checkIfImageExists = async (url) => {
   await fetch(url)
       .then(res => {
       if(res.status == 404){
        
         return false
       }else{
       
         return true
      }
    })
   .catch(err=>{
    console.log('Image does not exist');
    console.log(err);
     return false})
   }

  const uploadImage = async () => {

    setIsLoading(true);
    if (selectedImage || downloadedImgURL) {

          const response = await fetch(selectedImage ? selectedImage : downloadedImgURL);
          const blob = await response.blob()
        
        const storageRef = firebase.storage().ref();
        const thisRef = storageRef.child('ProfilePhoto/myPhoto');
        thisRef.put(blob)
        .then(function (snapshot) {
        console.log('Uploaded a blob or file!');
        thisRef.getDownloadURL().then((url, error) => {
          if (error) {
            setIsLoading(false);
            Alert.alert('Failed to upload image. Please try again!', [
              { text: 'Okay' }
            ]);

          }else{
            updateProfile(url)
          }
         
        })
          
        }); 
    }else{
      Alert.alert('Select the Image to upload.', [
        { text: 'Okay' }
      ]);
      
    } 
  };

  const updateProfile = async (imgURL) => {

    console.log('UPDATE');
    // const userData = await AsyncStorage.getItem('userData');
    // if (!userData) {
     
    //   props.navigation.navigate('Auth');
    //   return;
    // }
    // const transformedData = JSON.parse(userData);
    // const { token, userId, expiryDate } = transformedData;
   
    // fetch(`https://identitytoolkit.googleapis.com/v1/accounts:update?key=AIzaSyBm2c8bPqAdAH4ZW4esOUG40OXvhuzTr0E`, {
    //   method:'POST',
    //   headers: {
    //   Accept:'application/json',
    //   'Content-Type':'application/json',
    //  },
    //   body:JSON.stringify({
    //    "id_token":token,
    //    "displayName": name,
    //    'photoUrl' : imgURL
    //    }),
    //   })
    //  .then((response) => response.json())
    //  .then((responseData) => {
       setIsLoading(false);
      dispatch(profileActions.updateProfile(name,imgURL));
      props.navigation.goBack(null);
  //  })
  //  .done();
  }

  const editProfilePressed = async () => {

    if (((selectedImage && selectedImage.length > 0) || (downloadedImgURL && downloadedImgURL.length > 0)) ){
     
      uploadImage();

    }else if (name && name.length > 0){

      updateProfile("");
    } else{

      Alert.alert('Wrong input!', 'Please check the errors in the form.', [
     { text: 'Okay' }
     ]);
  }
 };

  handlePicAvatar = useCallback(async () => {

    setIsTyping(false);
    const { status: cameraRollPerm } = await Permissions.askAsync(
      Permissions.CAMERA_ROLL
    );

    if (cameraRollPerm === "granted") {
    let pickerResult = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      allowsEditing: true,
      aspect: [4, 3],
      quality: 1,
    });

    if (pickerResult.cancelled === true) {
      return;
    }

    setSelectedImage( pickerResult.uri);
    setDownloadedImgURL('');
  }else{

    Alert.alert(
      'Insufficient permissions!',
      'You need to grant camera permissions to use this app.',
      [{ text: 'Okay' }]
    );
  }
  });

  if (usersProfile && downloadedImgURL.length == 0) {
    
     if (usersProfile.imageUrl && (checkIfImageExists(usersProfile.imageUrl))) {
        setDownloadedImgURL(usersProfile.imageUrl);
    }
  }

  console.log('selectedimg');
  console.log(selectedImage); 
  console.log('downloadeimgurl');
  console.log(downloadedImgURL); 


    return (
    
      <ScrollView>
        <View style={styles.form}>
        <View style={{alignItems: 'center'}}>
          <TouchableOpacity style={styles.avatarPlaceholder} onPress={handlePicAvatar} >
        
          <Image style={styles.avatar} source={{ uri: selectedImage ? selectedImage : downloadedImgURL? downloadedImgURL : null }} /> 
         
          {
           (!(downloadedImgURL && downloadedImgURL.length > 0) && !(selectedImage && selectedImage.length > 0) )&&
           <Ionicons
           name = "ios-add"
           size = {40}
           color = "#FFF"
           style = {{marginTop: 2, marginLeft: 6}}
         >
         </Ionicons>
}
          </TouchableOpacity>
          {isLoading && 
                <ActivityIndicator size="small" color={Colors.primary} />
           }
        <Text style={styles.label}>Name</Text>
        <TextInput
          style={styles.input}
          returnKeyType="next"
          onChangeText={text => 
            {setName(text)
             setIsTyping(true)
            }
          }
          value={name}
          placeholder="Name"
          defaultValue = {usersProfile ? usersProfile.name : ''}
        />
      </View>
        </View>
      </ScrollView>
  );
  
};

const styles = StyleSheet.create({
  form: {
    margin: 20
  },

  label: {
    fontFamily: 'open-sans-bold',
    marginVertical: 20,
    fontSize : 18
  },
  input: {
    paddingHorizontal: 2,
    paddingVertical: 20,
    borderBottomColor: '#ccc',
    borderBottomWidth: 1,
    fontSize : 20,
    width : '90%',
    textAlign:'center'
  },
  errorContainer: {
    marginVertical: 5
  },
  errorText: {
    fontFamily: 'open-sans',
    color: 'red',
    fontSize: 13
  },
  avatarPlaceholder: {
   
    width: 160,
    height :160,
    borderRadius: 80,
    marginTop: 10,
    backgroundColor: '#E1E2E6',
    justifyContent: 'center',
    alignItems: 'center'
  },
  avatar : {
    width: 160,
    height :160,
    borderRadius: 80,
    position: 'absolute',
  }
 
});

EditProfile.navigationOptions  = navData => {
  const submitFn = navData.navigation.getParam('submit');

  return {
    headerTitle: 'Edit Profile',
    headerLeft: (
      <HeaderButtons HeaderButtonComponent={HeaderButton}>
        <Item
          title="Menu"
          iconName={Platform.OS === 'android' ? 'md-menu' : 'ios-menu'}
          onPress={() => {
            navData.navigation.toggleDrawer();
          }}
        />
      </HeaderButtons>
    ),
    headerRight: (
      <HeaderButtons HeaderButtonComponent={HeaderButton}>
        <Item
          title="Save"
          iconName={
            Platform.OS === 'android' ? 'md-checkmark' : 'ios-checkmark'
          }
          onPress={submitFn}
        />
      </HeaderButtons>
    )
  };
};

export default EditProfile;