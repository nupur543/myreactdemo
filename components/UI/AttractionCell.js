import React from 'react';
import { View, Text, Image, StyleSheet, TouchableOpacity, Linking } from 'react-native';
import { Ionicons } from '@expo/vector-icons';

const AttractionCell = props => {

    let attraction = props.attraction
  
     // console.log("Attraction ");
  // console.log(attractions);
  // console.log("Attraction URL");
  //console.log(props.onSelect);
  return (
    <View style={styles.titleRecent}>
       <View style={styles.touchable}>
       <TouchableOpacity onLongPress ={props.onSelect} useForeground>
    <Image style={styles.image} source={{ uri: props.image }} />
    <Text style={styles.price}>{`${attraction.name}`}</Text>
    <Text style={styles.description}
    onPress={() => Linking.openURL(`${attraction.url ? attraction.url : attraction.eventURL }`)}>
    Book the tickets!!</Text>
    </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
    infoContainer: {
        marginLeft: 25,
        width: 250,
        justifyContent: 'center',
        alignItems: 'flex-start'
      },
      titleRecent: {
        flex: 1,
        marginTop:15,
        textAlign: "center",
        alignItems: "center"
      },
      title: {
        fontSize: 19
      },
      image: {
        width: '100%',
        height: 300
      },
      actions: {
        marginVertical: 10,
        alignItems: 'center'
      },
      price: {
        fontSize: 20,
        color: '#888',
        textAlign: 'center',
        marginVertical: 20,
        fontFamily: 'open-sans-bold'
      },
      description: {
        fontFamily: 'open-sans-bold',
        fontSize: 17,
        textAlign: 'center',
        color: 'blue',
        marginHorizontal: 20,
        marginVertical: 10
      }
});

export default AttractionCell;
