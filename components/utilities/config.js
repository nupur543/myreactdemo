import Firebase from "firebase";
import 'firebase/storage';
import {
  AsyncStorage
} from 'react-native';

const firebaseConfig = {
    apiKey: "AIzaSyBm2c8bPqAdAH4ZW4esOUG40OXvhuzTr0E",
    authDomain: "demoreact-afe9d.firebaseapp.com",
    databaseURL: "https://demoreact-afe9d.firebaseio.com",
    projectId: "demoreact-afe9d",
    storageBucket: "demoreact-afe9d.appspot.com",
    messagingSenderId: "1059796479978",
    appId: "1:1059796479978:web:5ce256152c3b4a34244c93"
  };

  //Firebase.initializeApp(firebaseConfig);

  export const getUserId = async () => {

  const userData = await AsyncStorage.getItem('userData');
  if (!userData) {
    setIsLoading(false);
    props.navigation.navigate('Auth');
    return;
  }
 // console.log('config Async id')
  const transformedData = JSON.parse(userData);
  const { token, userId, expiryDate } = transformedData;
  //console.log(userId)
  return userId
};
const app = Firebase.initializeApp(firebaseConfig);
export const db = app.database();
export const storageRef = Firebase.storage();
export const user = Firebase.auth().currentUser;
//export const uid = user.uid ;
export const timeStamp = Date.now() ;

export default Firebase