import { SQLite } from 'expo-sqlite';

const db = SQLite.openDatabase('places.db');

export const init = () => {
  
  const promise = new Promise((resolve, reject) => {
    db.transaction(tx => {
      tx.executeSql(
        'CREATE TABLE IF NOT EXISTS favourites (userId TEXT NOT NULL,id TEXT PRIMARY KEY NOT NULL, name TEXT NOT NULL, imageUri TEXT NOT NULL, url TEXT NOT NULL);',
        [],
        () => {
          resolve();
        },
        (_, err) => {
          reject(err);
        }
      );
    });
  });
  return promise;
};

export const insertAttraction = (userId,eventId,name, imageUri, url) => {
    const promise = new Promise((resolve, reject) => {
        db.transaction(tx => {
          tx.executeSql(
            `INSERT INTO favourites (userId,id, name, imageUri, url) VALUES (?,?,?, ?, ?);`,
            [userId,eventId, name, imageUri, url],
            (_, result) => {
              resolve(result);
            },
            (_, err) => {
              reject(err);
            }
          );
        });
      });
      return promise;
};

export const fetchAttractions = () => {
    const promise = new Promise((resolve, reject) => {
        db.transaction(tx => {
          tx.executeSql(
            'SELECT * FROM favourites',
            [],
            (_, result) => {
              //console.log("fetchAttractions");
              resolve(result);
            },
            (_, err) => {
              reject(err);
            }
          );
        });
      });
      return promise;
};

export const deleteAttractions = (eventId) => {
  const promise = new Promise((resolve, reject) => {
      db.transaction(tx => {
        tx.executeSql(
          'DELETE FROM  favourites where id=?',
          [eventId],
          (_, result) => {
            console.log("deleteAttraction");
            resolve(result);
          },
          (_, err) => {
            reject(err);
          }
        );
      });
    });
    return promise;
};

