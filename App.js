import React, { useState } from 'react';
import { createStore, combineReducers, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import { AppLoading } from 'expo';
import * as Font from 'expo-font';
import ReduxThunk from 'redux-thunk';
import Instabug from 'instabug-reactnative';

import favouriteReducer from './store/reducers/favourites';
import DrawerNavigator from './navigation/ShopNavigator';
import { init } from './components/utilities/db';
import postsReducer from './store/reducers/posts';
import authReducer from './store/reducers/auth';
import profileReducer from './store/reducers/profile';

init()
  .then(() => {
    console.log('Initialized database');
  })
  .catch(err => {
    console.log('Initializing db failed.');
    console.log(err);
  });

const rootReducer = combineReducers({
  fav: favouriteReducer,
   auth: authReducer,
  posts: postsReducer,
  profile: profileReducer
});
const store = createStore(rootReducer, applyMiddleware(ReduxThunk));

const fetchFonts = () => {
  return Font.loadAsync({
    'open-sans': require('./assets/fonts/OpenSans-Regular.ttf'),
    'open-sans-bold': require('./assets/fonts/OpenSans-Bold.ttf')
  });
};

export default function App() {
 
  const [fontLoaded, setFontLoaded] = useState(false);
  console.ignoredYellowBox = [
    'Setting a timer'
]

  if (!fontLoaded) {
    return (
      <AppLoading
        startAsync={fetchFonts}
        onFinish={() => {
          setFontLoaded(true);
        }}
      />
    );
  }
  return (
    <Provider store={store}>
    <DrawerNavigator />
  </Provider>
  );
}
