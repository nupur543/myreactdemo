import Post from '../../models/post';
import Comment from '../../models/comment';
import {db} from '../../components/utilities/config' 

export const CREATE_POST = 'CREATE_POST';
export const UPDATE_LIKE = 'UPDATE_LIKE';
export const ADD_COMMENT = 'ADD_COMMENT';
export const SET_POST = 'SET_POST';
export const DELETE_POST = 'DELETE_POST';

export const fetchPosts = (userId) => {
 
  return async (dispatch, getState) => {
    // any async code you want!
    try {
      const response = await fetch(
        'https://demoreact-afe9d.firebaseio.com/posts.json'
      );

      if (!response.ok) {
        throw new Error('Something went wrong!');
      }

      const resData = await response.json();
      const loadedPosts = [];

      for (const user_id in resData) {
       
        for (const key in resData[user_id]) {
          let postData = resData[user_id][key];
        
          const comments = [];
          if (postData.comments){
            for (const comment in postData.comments) {
              
              let commentObj = postData.comments[comment];
              Object.entries(commentObj).map((t,k) => {
                let parsed = new Comment(t[0],t[1]);
               comments.push(parsed);
              });
              
          }
          }

         
          loadedPosts.push(
            new Post(
              postData.id,
              user_id,
              postData.name,
              postData.imageUrl,
              postData.description,
              postData.likedBy,
              comments
            )
          );
        }
       // console.log('loadedPosts Action');
       // console.log(loadedPosts);
      }

      dispatch({ type: SET_POST, posts: loadedPosts, userId: userId });
    } catch (err) {
      // send to custom analytics server
      throw err;
    }
  };
};

export const createPost = (id, userId, name, description, imageUrl) => {
  let empty = ["null"];
   return async dispatch => {
   
    const response = await fetch(
      `https://demoreact-afe9d.firebaseio.com/posts/${userId}.json`,
      {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          id,
          userId,
          name,
          description,
          imageUrl,
          likedBy: empty
        })
      }
    );

    const resData = await response.json();
    //console.log('Post Created');
    //console.log(resData);
    dispatch({
      type: CREATE_POST,
      postData: {
        id,
       userId,
        name,
        description,
        imageUrl,
        empty
      }
    });
  };
};

export const addComment = (userid, postId, postedBy, comment) => {

  let newComnt = {};
  return async dispatch => {

    console.log('Action addComment');
//Get userename
    db.ref('users')
    .child(postedBy)
    .once("value")
    .then(data => {
      let profile = data.val();
     
      newComnt = new Comment(profile.name,comment);
     //Post update
      db.ref('posts')
    .child(userid)
    .once("value")
    .then(data => {
      // console.log('Commented snapshot');
      // console.log(data);
    
      data.forEach(snapshot => {

        let key = snapshot.key;
        let val = snapshot.val();

        if (val.id == postId){
          // console.log('Commented POST');
          // console.log(val);
          var commetObj = {};
          commetObj;
          db.ref('posts')
          .child(userid).child(`${key}/comments`).push(
            {
              [`${newComnt.username}`] : comment
            }
             ).then( () => {

              console.log('Comment Updated by');
              console.log(newComnt);
              dispatch({
                type: ADD_COMMENT,
                pid: postId,
                newComment : newComnt
           });


             });
        }
      });

    });
      

    });
    

   
  };
};


export const deletePost = (userId, postId) => {
  return async dispatch => {
    let usersPosts = db.ref(`posts/`).child(`${userId}/`);

    usersPosts.on('value',  function(snapshot){
     snapshot.forEach(userSnapshot => {
         if ( userSnapshot.val().id == postId ){
       
          usersPosts.child(userSnapshot.key).remove();
         }
   
      dispatch({
     type: DELETE_POST,
     pid: postId,
     uid: userId
   });

});
})
};
}

export const likePost = (postId, userId, arrUsers) => {

  return async dispatch => {

  const newLikedUsers = arrUsers.includes(userId) ? 
  (arrUsers.filter(user => user !== userId)) :
  arrUsers.concat(userId)
  if (!newLikedUsers.includes('null')) {
    newLikedUsers.push('null');
  }
  //console.log(newLikedUsers); 
  
   db.ref(`posts/`).on('value',  function(data)  {
    data.forEach(userSnapshot => {
      let userKey = userSnapshot.key;
      if (userKey !== userId) {
       // console.log('postId ');
       // console.log(postId);
       let liked = db.ref(`posts/`).child(`${userKey}/`);
       liked.on('value',  function(snapshot){
        snapshot.forEach(userSnapshot => {
            if ( userSnapshot.val().id == postId ){
              console.log('newLikedUsers');
              console.log(newLikedUsers);
              liked.child(userSnapshot.key).update(
             {
                 likedBy: newLikedUsers
             }
              );
            }
      
         dispatch({
        type: UPDATE_LIKE,
        pid: postId,
        uid: userId,
        updatedLikedByUsers: newLikedUsers
      });

         // liked.child(userSnapshot.key)
        });
      
       }); 
      }
    });
  });
   
};
};
