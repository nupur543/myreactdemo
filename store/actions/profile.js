import {
    Alert,
    AsyncStorage
  } from 'react-native';

import profile from '../../models/profile';
import {db, firebase} from '../../components/utilities/config' 

export const UPDATE_PROFILE = 'UPDATE_PROFILE';
export const GET_PROFILE = 'GET_PROFILE';

export const fetchProfile = (userId) => {
   
  return async dispatch => {

 db.ref(`users/`).orderByChild('userId').once('value', 
 function(data) {

  const allprofl = [];
  let userProfl = {};
  
   data.forEach( childShapshot => {
    
    let profl = childShapshot.val();
   // console.log('profl');
    //console.log(profl);

    allprofl.push(profl);
     if(profl.userId === userId){
      userProfl = profl;
     }
 }
 );
 dispatch({
  type: GET_PROFILE,
  userID: userId,
  profileData: userProfl,
  allLikedUsers: allprofl
});

});
  
 
 };
};

  export const createProfile = async (name) => {

    const userData = await AsyncStorage.getItem('userData');
    const transformedData = JSON.parse(userData);
    const { token, userId, expiryDate } = transformedData;

   // console.log('CREATE PROFILE');
   
     db.ref(`users/${userId}`).set({
    userId: userId ,
    name : name,
    imageUrl: "",
    }).then(confirmResult => {
      let result = confirmResult;
      console.log(result);
    });
  }

  export const updateProfile = (name, imageURL) => {
   
    return async dispatch => {
      const userData = await AsyncStorage.getItem('userData');
      const transformedData = JSON.parse(userData);
      const { token, userId, expiryDate } = transformedData;
  
   db.ref(`users/${userId}`).update({
     name : name,
     imageUrl: imageURL
     });
    
     dispatch({
       type: UPDATE_PROFILE,
       profileData: {
         name,
         imageURL
       }
     });
   };
  };
  