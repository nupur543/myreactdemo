import {
  Alert,
  AsyncStorage
} from 'react-native';
import { insertAttraction, fetchAttractions, checkIfEventExists, deleteAttractions } from '../../components/utilities/db';

export const ADD_EVENT = 'ADD_EVENT';
export const SET_EVENTS = 'SET_EVENTS';
export const DELETE_EVENT = 'DELETE_EVENT';

export const addEvent = (eventId, name, imgURL, eventURL) => {
  return async dispatch => {
    const userData = await AsyncStorage.getItem('userData');
    const transformedData = JSON.parse(userData);
    const { token, userId, expiryDate } = transformedData;

    try {
      
      const dbResult = await insertAttraction(
        userId,
        eventId,
        name,
        imgURL,
        eventURL
      );
      console.log(dbResult);
      dispatch({ type: ADD_EVENT, eventData: {userId: userId, id: eventId, name: name, imgUrl: imgURL,eventURL: eventURL } });
    } catch (err) {
      console.log(err);
      throw err;
    }
  };
};

export const loadAttractions = () => {
   
    return async dispatch => {
      const userData = await AsyncStorage.getItem('userData');
      const transformedData = JSON.parse(userData);
      const { token, userId, expiryDate } = transformedData;
  
        try {
            const dbResult = await fetchAttractions();
            //console.log(dbResult.rows._array);
            dispatch({ type: SET_EVENTS,userId: userId, events: dbResult.rows._array });
        } catch (err) {
            throw err;
        }
    };
};

export const deleteAttraction = (eventId) => {
   
  return async dispatch => {
      try {
          const dbResult = await deleteAttractions(eventId);
          console.log(dbResult);
          dispatch({ type: DELETE_EVENT, pid: eventId });
      } catch (err) {
          throw err;
      }
  };
};