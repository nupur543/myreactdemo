import { AsyncStorage } from 'react-native';
import { useDispatch } from 'react-redux';

import {db, firebase} from '../../components/utilities/config' 

export const SIGNUP = 'SIGNUP';
export const LOGIN = 'LOGIN';
export const AUTHENTICATE = 'AUTHENTICATE';
export const LOGOUT = 'LOGOUT';

let timer;

export const authenticate = (userId, token, expiryTime) => {
  return dispatch => {
    dispatch(setLogoutTimer(expiryTime));
    dispatch({ type: AUTHENTICATE, userId: userId, token: token });
  };
};

export const signup = (email, password, name) => {
  return async dispatch => {
    const response = await fetch(
      'https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=AIzaSyBm2c8bPqAdAH4ZW4esOUG40OXvhuzTr0E',
      {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          email: email,
          password: password,
          name: name,
          returnSecureToken: true
        })
      }
    );

    if (!response.ok) {
      throw new Error('Something went wrong!');
    }

    const resData = await response.json();
    //addProfileDatabase(resData,name);
    saveDataToStorage(resData);
    console.log('LocalId');
    console.log(resData.localId);
    db.ref(`users/${resData.localId}`).set({
      userId: resData.localId ,
      name : name,
      imageUrl: "",
      }).then(confirmResult => {
        let result = confirmResult;
        console.log(result);
        
        dispatch({ type: SIGNUP ,
          token: resData.idToken,
          userId:  resData.localId,
          name: name});
      });
  };
};

export const addProfileDatabase = (resData,name) => {
  return dispatch => {
  console.log('addProfileDatabase');
   console.log('LocalId');
    console.log(resData.localId);
 // const dispatch = useDispatch();

   
  };
}

export const login = (email, password) => {
  
    return async dispatch => {
      const response = await fetch(
         'https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyBm2c8bPqAdAH4ZW4esOUG40OXvhuzTr0E',
        {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json'
          }
          ,
          body: JSON.stringify({
            email: email,
            password: password,
            returnSecureToken: true
          })

        }
      );
  
      if (!response.ok) {
        console.log(response.Error);
        throw new Error('Something went wrong!');
      }
  
      const resData = await response.json();
     // console.log('Attractions response');
     // console.log(resData);
     saveDataToStorage(resData);
      dispatch({ type: LOGIN });
    };
  };

  export const logout = () => {
    console.log("Auth action logout");
    clearLogoutTimer();
    AsyncStorage.removeItem('userData');
    return { type: LOGOUT };
  };
  
  const clearLogoutTimer = () => {
    if (timer) {
      clearTimeout(timer);
    }
  };
  
  const setLogoutTimer = expirationTime => {
    return dispatch => {
      timer = setTimeout(() => {
        dispatch(logout());
      }, expirationTime);
    };
  };

  const saveDataToStorage = (resData) => {
  
    const expirationDate = new Date(
      new Date().getTime() + parseInt(resData.expiresIn) * 1000
    );

    AsyncStorage.setItem(
      'userData',
      JSON.stringify({
        token: resData.idToken,
        userId: resData.localId,
        expiryDate: expirationDate.toISOString()
      })
    );
  };
  