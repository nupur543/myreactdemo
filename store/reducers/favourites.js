import Event from '../../models/event';
import { ADD_EVENT, SET_EVENTS, DELETE_EVENT } from '../../store/actions/favourites';

const initialState = {
    favourites :  []
};

export default  favouriteReducer = (state = initialState, action) => {
   
    switch (action.type) {
        case SET_EVENTS: 
        const myEvents =  action.events.filter(
          pl => pl.userId == action.userId
        )

        //console.log('myEvents');
       // console.log(action.events);
     return {
            ...state,
            favourites:myEvents.map(
              pl => new Event(pl.userId,pl.id, pl.name, pl.imageUri, pl.url)
            )
          };
        case ADD_EVENT:
          const newEvent = new Event(
            action.eventData.userId,
            action.eventData.id,
            action.eventData.name,
            action.eventData.imgUrl,
            action.eventData.eventURL
          );
         
          return {
            ...state,
            favourites: state.favourites.concat(newEvent)
          };

          case DELETE_EVENT:
            return {
              ...state,
              favourites: state.favourites.filter(
                event => event.id !== action.pid
              )
            };
        
        default:
          return state;
      }
};


