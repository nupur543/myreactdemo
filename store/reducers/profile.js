
import {
    UPDATE_PROFILE,
   GET_PROFILE
  } from '../actions/profile';
  import Profile from '../../models/profile';
  
  const initialState = {
    profileData: null,
    likedProfiles: []
  };
  
  export default (state = initialState, action) => {
   
   // console.log(action.type);
    switch (action.type) {
     
      case GET_PROFILE:
      const allUsers =  action.allLikedUsers.map( 
          user =>
          new Profile(
            user.userId,
            user.name,
            user.imageUrl,
        ));
      const fetchedProfile =  new Profile(
        action.profileData.userId,
        action.profileData.name,
        action.profileData.imageUrl,
    );
        
      
        return {
          ...state,
          profileData: fetchedProfile,
          likedProfiles: allUsers
        };
      case UPDATE_PROFILE:

        const postIndex = state.likedProfiles.findIndex(
          prod => prod.userId === action.profileData.userId
        );
  
        const updatedProfile = new Profile(
            action.profileData.userId,
            action.profileData.name,
            action.profileData.imageUrl,
          );

          const updatedAvailablePosts = [...state.likedProfiles];
          // console.log('updatedAvailablePosts');
           // console.log(updatedAvailablePosts);
            
            updatedAvailablePosts[postIndex] = updatedProfile;

          console.log('updatedProfile');
          console.log(updatedProfile);

        return {
          ...state,
          profileData: updatedProfile,
          likedProfiles: updatedAvailablePosts
        };
     
    }
    return state;
  };
  