
import {
  DELETE_POST,
  CREATE_POST,
  UPDATE_LIKE,
  ADD_COMMENT,
  SET_POST
} from '../actions/posts';
import Post from '../../models/post';

const initialState = {
  availablePosts: [],
  userPosts: []
};

export default (state = initialState, action) => {
 // console.log('REducer called');
 // console.log(action.type);
  switch (action.type) {
    case SET_POST:

     const sortedposts =  action.posts.sort(function(a,b){
        (b.id) > (a.id);
     });
     
      //console.log('sortedPosts');
      //console.log(sortedposts);
      return {
        availablePosts: sortedposts,
        userPosts: sortedposts.filter(prod => prod.userId === action.userId)
      };
    case CREATE_POST:
      const newPost = new Post(
        action.postData.id,
        action.postData.userId,
        action.postData.name,
        action.postData.imageUrl,
        action.postData.description,
        action.postData.likedUsers,
        state.postData.comments ? state.postData.comments : []
      );
      console.log('CREATE_POST');
      console.log(newPost);

      return {
        ...state,
        availablePosts: state.availablePosts.concat(newPost),
        userPosts: state.userPosts.concat(newPost)
      };

    case UPDATE_LIKE:
      const postIndex = state.availablePosts.findIndex(
        prod => prod.id === action.pid
      );

      const updatedPost =  new Post(
        state.availablePosts[postIndex].id,
        state.availablePosts[postIndex].userId,
        state.availablePosts[postIndex].name,
        state.availablePosts[postIndex].imageUrl,
        state.availablePosts[postIndex].description,
        action.updatedLikedByUsers,
        state.availablePosts[postIndex].comments
      );

      const updatedAvailablePosts = [...state.availablePosts];
      updatedAvailablePosts[postIndex] = updatedPost;
      
      return {
        ...state,
        availablePosts: updatedAvailablePosts,
        userPosts: state.userPosts
      };

case ADD_COMMENT:
  const postIndex2 = state.availablePosts.findIndex(
    prod => prod.id === action.pid
  );

  const updatedPost2 =  new Post(
    state.availablePosts[postIndex2].id,
    state.availablePosts[postIndex2].userId,
    state.availablePosts[postIndex2].name,
    state.availablePosts[postIndex2].imageUrl,
    state.availablePosts[postIndex2].description,
    state.availablePosts[postIndex2].likedUsers,
    state.availablePosts[postIndex2].comments.concat(action.newComment)
  );

  const updatedAvailablePosts2 = [...state.availablePosts];
  updatedAvailablePosts2[postIndex2] = updatedPost2;

  console.log('ADD_COMMENT');
  console.log(updatedAvailablePosts2);
  
  return {
    ...state,
    availablePosts: updatedAvailablePosts2,
    userPosts: state.userPosts
  };

    case DELETE_POST:
      return {
        ...state,
        userPosts: state.userPosts.filter(
          product => product.id !== action.pid
        ),
        availablePosts: state.availablePosts.filter(
          product => product.id !== action.pid
        )
      };
  }
  return state;
};
