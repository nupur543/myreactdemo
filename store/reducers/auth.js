import { AUTHENTICATE, LOGOUT, SIGNUP } from '../actions/auth';

import * as pofileActions from '../actions/profile';
  

const initialState = {
  token: null,
  userId: null,
  name: null
};

export default (state = initialState, action) => {
  switch (action.type) {
    case AUTHENTICATE:
    
      return {
        token: action.token,
        userId: action.userId,
        name: state.name
      };
    case LOGOUT:
      return initialState;
    case SIGNUP:
     
      return {
        token: action.token,
        userId: action.userId,
        name: action.name
      };
    default:
      return state;
  }
};
