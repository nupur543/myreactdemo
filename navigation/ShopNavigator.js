import React from 'react';
import {
  createStackNavigator,
  createDrawerNavigator,
  createSwitchNavigator,
  createAppContainer,
  DrawerItems
} from 'react-navigation';

import { Platform, SafeAreaView, Button, View } from 'react-native';
import { useDispatch } from 'react-redux';

import AuthScreen from '../screens/user/AuthScreen';
import DetailScreen from '../screens/user/DetailScreen';
import HomeScreen from '../screens/user/HomeScreen';
import StartupScreen from '../screens/StartupScreen';
import EditProfile from '../screens/EditProfile';
import MyFavourites from '../screens/MyFavourites';
import PostsScreen from '../screens/Posts/PostsScreen';
import NewPost from '../screens/Posts/NewPost';
import LikedByUsers from '../screens/Posts/LikedByUsers';
import UserProfile from '../screens/Posts/UserProfile';
import Comments from '../screens/Posts/Comments';
import Colors from '../constants/Colors';
import * as authActions from '../store/actions/auth';

const defaultNavOptions = {
  headerStyle: {
    backgroundColor: Platform.OS === 'android' ? Colors.primary : ''
  },
  headerTitleStyle: {
    fontFamily: 'open-sans-bold'
  },
  headerBackTitleStyle: {
    fontFamily: 'open-sans'
  },
  headerTintColor: Platform.OS === 'android' ? 'white' : Colors.primary
};


const AuthNavigator = createStackNavigator(
  {
    Auth: AuthScreen
  },
  {
    defaultNavigationOptions: defaultNavOptions
  }
);


const HomeBaseNavigator = createStackNavigator(
  {
    Home: HomeScreen,
    Detail: DetailScreen
  },
  {
    defaultNavigationOptions: defaultNavOptions
  }
);

const EditProflNavigator = createStackNavigator(
  {
    Edit: EditProfile
  },
  {
    defaultNavigationOptions: defaultNavOptions
  }
);

const MyFavouriteNavigator = createStackNavigator(
  {
    Favourites: MyFavourites
  },
  {
    defaultNavigationOptions: defaultNavOptions
  }
);

const PostsNavigator = createStackNavigator(
  {
    Posts: PostsScreen,
    NewPost: NewPost,
    LikedByUser: LikedByUsers,
    Profile: UserProfile,
    Comments: Comments
  },
  {
    defaultNavigationOptions: defaultNavOptions
  }
);

const DrawerNavigator = createDrawerNavigator({
  
  Screen1: {
    screen: HomeBaseNavigator,
    navigationOptions: {
      drawerLabel: 'Search Events',
    },
  },
  Screen2: {

    screen: EditProflNavigator,
    navigationOptions: {
      drawerLabel: 'My Account',
    },
  },
  Screen3: {

    screen: MyFavouriteNavigator,
    navigationOptions: {
      drawerLabel: 'My Favourites',
    },
  },
  Screen4: {

    screen: PostsNavigator,
    navigationOptions: {
      drawerLabel: 'Posts',
    },
  },
},
  {
    
    contentOptions: {
       activeTintColor: Colors.primary
    },
    contentComponent: props => {
      const dispatch = useDispatch();
      return (
        <View style={{ flex: 1, paddingTop: 20, marginTop: 40 }}>
          <SafeAreaView forceInset={{ top: 'always', horizontal: 'never' }}>
            <DrawerItems {...props} />
            <Button
              title="Logout"
              color={Colors.primary}
              onPress={() => {
                dispatch(authActions.logout());
                 props.navigation.navigate('Auth');
              }}
            />
          </SafeAreaView>
        </View>
      );
    }
  }
);

const MainNavigator = createSwitchNavigator({
  Startup: StartupScreen,
  Auth: AuthNavigator,
  Home: DrawerNavigator
});

export default createAppContainer(MainNavigator);
